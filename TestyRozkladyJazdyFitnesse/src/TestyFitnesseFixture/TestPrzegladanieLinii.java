/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestyFitnesseFixture;

import RozkladyJazdy.Model.Linia;
import fit.ColumnFixture;


public class TestPrzegladanieLinii  extends ColumnFixture  {
    String nazwaLinii;
    
    // funkcja zależna od TestDodawaniePrzystankow
    public int iloscPrzystankowPrzypisanychDoLinii(){
        Linia l = SetUp.aplikacja.szukajLinie(nazwaLinii);
        
        if(l == null || l.pobierzListePrzystankow() == null){
            return 0;
        }
        
        return l.pobierzListePrzystankow().size();
    }
}
