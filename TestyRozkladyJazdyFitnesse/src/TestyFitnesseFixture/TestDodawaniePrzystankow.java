/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestyFitnesseFixture;

import RozkladyJazdy.Model.Linia;
import fit.ColumnFixture;
import java.util.IllegalFormatCodePointException;

public class TestDodawaniePrzystankow extends ColumnFixture {
    String nazwaNowegoPrzystanku;
    String liniaDoKtorejDodacPrzystanek;
    
    public boolean dodajPrzystanek() {
        Linia linia = SetUp.aplikacja.szukajLinie(liniaDoKtorejDodacPrzystanek);
        int l1 = linia.pobierzListePrzystankow().size();
        int s1 = iloscPrzystankow();
        SetUp.aplikacja.dodajPrzystanek(nazwaNowegoPrzystanku, liniaDoKtorejDodacPrzystanek);
        int l2 = linia.pobierzListePrzystankow().size();
        
        if(l1!=l2)return true;
        
        int s2 = iloscPrzystankow();
        
        return s1 != s2;
    }

    public int iloscPrzystankow() {
        return SetUp.aplikacja.iloscPrzystankow();
    }
}
