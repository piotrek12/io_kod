/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestyFitnesseFixture;

import fit.ColumnFixture;

/**
 *
 * @author piotr
 */
public class TestDodawanieKomunikatu extends ColumnFixture{
    String nazwa;
    int id;
    
    public boolean dodajKomunikat(){
        int s1 = iloscKomunikatow();
        SetUp.aplikacja.dodajKomunikat(id, nazwa);
        int s2 = iloscKomunikatow();
        return s1!=s2;
    }
    
    public int iloscKomunikatow(){
        return SetUp.aplikacja.iloscKomunikatow();
    }
}
