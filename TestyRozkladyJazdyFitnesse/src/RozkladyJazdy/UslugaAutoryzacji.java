package RozkladyJazdy;

import RozkladyJazdy.Model.Uzytkownik;
import java.util.ArrayList;
import java.util.Iterator;

public class UslugaAutoryzacji {

    private boolean udzielonoDostepu;
    public ArrayList<Uzytkownik> uzytkownicy = new ArrayList<Uzytkownik>();

    public UslugaAutoryzacji() {
        udzielonoDostepu = false;
        
        uzytkownicy.add(new Uzytkownik("pracownik1", "12345"));
    }

    public void weryfikujKlucz(String login, String klucz) {
        Iterator it = uzytkownicy.iterator();

        while (it.hasNext()) {
            Uzytkownik uzytkownik = (Uzytkownik) it.next();
            String loginUzytkownika = uzytkownik.pobierzLogin();
            if (loginUzytkownika.equals(login)) {
                if (uzytkownik.porownajHaslo(klucz)) {
                    udzielonoDostepu = true;
                }
            }
        }
    }

    public boolean sprawdzDostep() {
        if (udzielonoDostepu == true) {
            return true;
        } else {
            return false;
        }
    }
    
    public void zamknijDostep()
    {
        udzielonoDostepu = false;
    }
}
