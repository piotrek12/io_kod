package RozkladyJazdy.Model;

public class Przystanek {

    private String nazwa;

    public Przystanek() {

    }

    public String pobierzNazwe() {
        return nazwa;
    }

    public void ustawNazwe(String nazwa) {
        this.nazwa = nazwa;
    }

}
