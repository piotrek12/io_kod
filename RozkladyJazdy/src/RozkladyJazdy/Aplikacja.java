package RozkladyJazdy;

import java.util.*;
import java.time.*;
import RozkladyJazdy.Model.Komunikat;
import RozkladyJazdy.Model.Linia;
import RozkladyJazdy.Model.Przystanek;
import RozkladyJazdy.Model.Uzytkownik;

public class Aplikacja {

    private ArrayList<Linia> linie = new ArrayList<>();
    private ArrayList<Przystanek> przystanki = new ArrayList<>();
    public UslugaAutoryzacji uslugaAutoryzacji = new UslugaAutoryzacji();
    private ArrayList<Komunikat> komunikaty = new ArrayList<>();

    static final String USER_ENTRANCE_MESSAGE
            = "=== Aplikacja RozkladyJazdy ============= \n"
            + "Wpisz numer komendy aby kontynuowac:\n"
            + "0. Zaloguj sie na konto pracownika\n"
            + "1. Wyswietl przystanki\n"
            + "2. Wyswietl linie\n"
            + "3. Wyswietl komunikaty\n"
            + "4. Wyswietl przystanki dla wybranej linii\n";

    static final String EMPLOYEE_ENTRANCE_MESSAGE
            = "=== Aplikacja RozkladyJazdy ============= \n"
            + "Wpisz numer komendy aby kontynuowac:\n"
            + "1. Wyswietl przystanki\n"
            + "2. Wyswietl linie\n"
            + "3. Wyswietl komunikaty\n"
            + "4. Wyswietl przystanki dla wybranej linii\n"
            + "5. Dodaj przystanek\n"
            + "6. Dodaj linie\n"
            + "7. Dodaj komunikat\n"
            + "8. Usun przystanek\n"
            + "9. Usun linie\n"
            + "10. Usun komunikat\n";

    public Aplikacja(){
        
    }
    
    public Aplikacja(UslugaAutoryzacji ua){
        uslugaAutoryzacji = ua;
    }
    
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        Aplikacja aplikacja = new Aplikacja();

        Uzytkownik uzytkownik = new Uzytkownik("admin", "admin");
        aplikacja.uslugaAutoryzacji.uzytkownicy.add(uzytkownik);

        Linia l1 = new Linia();
        l1.ustawNazwe("1");
        aplikacja.linie.add(l1);
        Linia l2 = new Linia();
        l2.ustawNazwe("2");
        aplikacja.linie.add(l2);
        Linia l3 = new Linia();
        l3.ustawNazwe("3");
        aplikacja.linie.add(l3);

        Przystanek p1 = new Przystanek();
        p1.ustawNazwe("Nowowiejska");
        aplikacja.przystanki.add(p1);
        Przystanek p2 = new Przystanek();
        p2.ustawNazwe("Reja");
        aplikacja.przystanki.add(p2);

        l1.dodajPrzystanek(p1);
        l1.dodajPrzystanek(p2);

        while (true) {
            String input = null;
            String nazwaPrzystanku = null;
            String nazwaLinii = null;
            String tresc = null;
            int ID = 0;

            if (aplikacja.uslugaAutoryzacji.sprawdzDostep() == false) {
                System.out.println(USER_ENTRANCE_MESSAGE);
            } else {
                System.out.println(EMPLOYEE_ENTRANCE_MESSAGE);
            }

            input = scanner.nextLine();

            switch (input) {
                case "1":
                    System.out.println("Zapisane przystanki:");
                    aplikacja.wyswietlWszystkiePrzystanki();
                    break;
                case "2":
                    System.out.println("Zapisane linie:");
                    aplikacja.wyswietlWszystkieLinie();
                    break;
                case "3":
                    System.out.println("Zapisane komunikaty:");
                    aplikacja.wyswietlWszystkieKomunikaty();
                    break;
                case "4":
                    System.out.println("Podaj nazwe linii:");
                    nazwaLinii = scanner.nextLine();
                    aplikacja.szukajLinie(nazwaLinii).wyswietlPrzystanki();
                    break;
            }

            if (aplikacja.uslugaAutoryzacji.sprawdzDostep() == false) {
                switch (input) {
                    case "0":
                        System.out.println("Podaj login:");
                        String login = scanner.nextLine();
                        System.out.println("Podaj klucz:");
                        String klucz = scanner.nextLine();
                        aplikacja.uslugaAutoryzacji.weryfikujKlucz(login, klucz);
                        break;
                }
            } else {
                switch (input) {
                    case "5":
                        System.out.println("Podaj nazwę przystanku:");
                        nazwaPrzystanku = scanner.nextLine();
                        System.out.println("Podaj nazwę linii:");
                        nazwaLinii = scanner.nextLine();
                        aplikacja.dodajPrzystanek(nazwaPrzystanku, nazwaLinii);
                        break;
                    case "6":
                        System.out.println("Podaj nazwę:");
                        nazwaLinii = scanner.nextLine();
                        aplikacja.dodajLinie(nazwaLinii);
                        break;
                    case "7":
                        System.out.println("Podaj treść:");
                        tresc = scanner.nextLine();
                        System.out.println("Podaj identyfikator");
                        ID = Integer.parseInt(scanner.nextLine());
                        aplikacja.dodajKomunikat(ID, tresc);
                        break;
                    case "8":
                        System.out.println("Podaj nazwę:");
                        nazwaPrzystanku = scanner.nextLine();
                        System.out.println("Podaj nazwę linii:");
                        nazwaLinii = scanner.nextLine();
                        aplikacja.usunPrzystanek(nazwaPrzystanku, nazwaLinii);
                        break;
                    case "9":
                        System.out.println("Podaj nazwę:");
                        nazwaLinii = scanner.nextLine();
                        aplikacja.usunLinie(nazwaLinii);
                        break;
                    case "10":
                        System.out.println("Podaj identyfikator");
                        ID = Integer.parseInt(scanner.nextLine());
                        aplikacja.usunKomunikat(ID);
                        break;

                }
            }
        }
    }

    public Przystanek szukajPrzystanek(String nazwa) {
        Iterator it = przystanki.iterator();

        while (it.hasNext()) {
            Przystanek przystanek = (Przystanek) it.next();
            String nazwaPrzystanku = przystanek.pobierzNazwe();
            if (nazwa.equalsIgnoreCase(nazwaPrzystanku) == true) {
                return przystanek;
            }
        }
        return null;
    }

    Linia szukajLinie(String nazwa) {
        Iterator it = linie.iterator();

        while (it.hasNext()) {
            Linia linia = (Linia) it.next();
            String nazwaLinii = linia.pobierzNazwe();
            if (nazwa.equalsIgnoreCase(nazwaLinii) == true) {
                return linia;
            }
        }
        return null;
    }

    void dodajPrzystanek(String nazwaPrzystanku, String nazwaLinii) {
        if (uslugaAutoryzacji.sprawdzDostep() == true) {
            Przystanek przystanek = szukajPrzystanek(nazwaPrzystanku);

            if (przystanek == null) {
                przystanek = new Przystanek();
                przystanek.ustawNazwe(nazwaPrzystanku);
                przystanki.add(przystanek);
            }

            Linia linia = szukajLinie(nazwaLinii);
            if (linia != null) {
                linia.dodajPrzystanek(przystanek);
            }
        }
    }

    void dodajLinie(String nazwaLinii) {
        Linia linia = new Linia();
        if (uslugaAutoryzacji.sprawdzDostep() == true) {
            if (szukajPrzystanek(nazwaLinii) == null) {
                linia.ustawNazwe(nazwaLinii);
                linie.add(linia);
            }
        }
    }

    void dodajKomunikat(int idKomunikatu, String trescKomunikatu) {
        Komunikat komunikat = new Komunikat();
        if (uslugaAutoryzacji.sprawdzDostep()) {
            if (trescKomunikatu != null && !trescKomunikatu.isEmpty()) {
                komunikat.ustawTresc(trescKomunikatu);
                komunikat.ustawIdentyfikator(idKomunikatu);
                LocalDateTime currentDate = LocalDateTime.now();
                komunikat.ustawDate(currentDate);
                komunikaty.add(komunikat);
            }
        }
    }

    void wyswietlWszystkiePrzystanki() {
        Iterator it = przystanki.iterator();
        while (it.hasNext()) {
            Przystanek przystanek = (Przystanek) it.next();
            System.out.println(przystanek.pobierzNazwe());
        }
    }
    
    public int iloscPrzystankow(){
        return przystanki.size();
    }

    void wyswietlWszystkieLinie() {
        Iterator it = linie.iterator();
        while (it.hasNext()) {
            Linia linia = (Linia) it.next();
            System.out.println(linia.pobierzNazwe());
        }
    }

    void wyswietlWszystkieKomunikaty() {
        Iterator it = komunikaty.iterator();
        while (it.hasNext()) {
            Komunikat komunikat = (Komunikat) it.next();
            System.out.println(komunikat.pobierzTresc());
        }
    }

    void usunPrzystanek(String nazwaPrzystanku, String nazwaLinii) {
        Iterator it = przystanki.iterator();
        while (it.hasNext()) {
            Przystanek przystanek = (Przystanek) it.next();
            if (nazwaPrzystanku.equalsIgnoreCase(przystanek.pobierzNazwe())) {
                it.remove();
                Linia linia = szukajLinie(nazwaLinii);
                linia.usunPrzystanek(przystanek);
            }

        }

    }

    void usunLinie(String nazwa) {
        Iterator it = linie.iterator();
        while (it.hasNext()) {
            Linia linia = (Linia) it.next();
            String nazwaLinii = linia.pobierzNazwe();
            if (nazwa.equalsIgnoreCase(nazwaLinii)) {
                it.remove();
            }
        }
    }

    void usunKomunikat(int identyfikator) {
        Iterator it = komunikaty.iterator();
        while (it.hasNext()) {
            Komunikat komunikat = (Komunikat) it.next();
            int idKomunikatu = komunikat.pobierzIdentyfikator();
            if (idKomunikatu == identyfikator) {
                it.remove();
            }
        }
    }
}
