package RozkladyJazdy.Model;

import java.time.LocalDateTime;

public class Komunikat {

    private String tresc;
    private LocalDateTime data;
    private int identyfikator;

    public Komunikat() {

    }

    public String pobierzTresc() {
        return tresc;
    }

    public void ustawTresc(String tresc) {
        this.tresc = tresc;
    }

    public LocalDateTime pobierzDate() {
        return data;
    }

    public void ustawDate(LocalDateTime data) {
        this.data = data;
    }

    public int pobierzIdentyfikator() {
        return identyfikator;
    }

    public void ustawIdentyfikator(int identyfikator) {
        this.identyfikator = identyfikator;
    }
}
