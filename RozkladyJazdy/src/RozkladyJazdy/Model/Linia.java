package RozkladyJazdy.Model;

import java.util.ArrayList;

public class Linia {

    private String nazwaLinii;
    private ArrayList<Przystanek> listaPrzystankow = new ArrayList<Przystanek>();

    public Linia() {

    }

    public String pobierzNazwe() {
        return nazwaLinii;
    }

    public void ustawNazwe(String nazwa) {
        nazwaLinii = nazwa;
    }

    public ArrayList<Przystanek> pobierzListePrzystankow() {
        return null;
    }

    public void ustawListePrzystankow(ArrayList<Przystanek> lista) {
        listaPrzystankow = lista;
    }
    
    public void dodajPrzystanek(Przystanek przystanek)
    {
        listaPrzystankow.add(przystanek);
    }
    
    public void usunPrzystanek(Przystanek przystanek) throws NullPointerException
    {
        if(przystanek == null)
            throw new NullPointerException();
        
        listaPrzystankow.remove(przystanek);
    }
    
    public void wyswietlPrzystanki()
    {
        for(Przystanek p:listaPrzystankow)
        {
            System.out.println(p.pobierzNazwe());
        }
    }
}
