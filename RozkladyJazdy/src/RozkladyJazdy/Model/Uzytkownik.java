package RozkladyJazdy.Model;

public class Uzytkownik {

    private String login;
    private String haslo;

    public Uzytkownik(String login, String haslo) {
        this.login = login;
        this.haslo = haslo;
    }

    public String pobierzLogin() {
        return login;
    }

    public boolean porownajHaslo(String haslo) {
        if (this.haslo.equals(haslo)) {
            return true;
        } else {
            return false;
        }
    }
}
