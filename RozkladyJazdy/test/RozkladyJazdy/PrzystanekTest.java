/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RozkladyJazdy;

import Suite.TestEntity;
import RozkladyJazdy.Model.Przystanek;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;

@Category({ TestEntity.class})

public class PrzystanekTest {
    static Przystanek instance;
    
    @BeforeClass
    public static void setUpClass() {
        instance = new Przystanek();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
        instance.ustawNazwe(null);
    }    

    /**
     * Test of pobierzNazwe method, of class Przystanek.
     */
    @Test
    public void testPobierzNazwe() {
        System.out.println("pobierzNazwe");
        
        assertEquals(null, instance.pobierzNazwe());
        instance.ustawNazwe("Przystanek1");
        String result = instance.pobierzNazwe();
        assertEquals(result, "Przystanek1");
    }

    /**
     * Test of ustawNazwe method, of class Przystanek.
     */
    @Test
    public void testUstawNazwe() {
        System.out.println("ustawNazwe");
        
        assertEquals(null, instance.pobierzNazwe());
        
        instance.ustawNazwe("Przystanek1");
        assertEquals("Przystanek1", instance.pobierzNazwe());
    }
    
}
