/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RozkladyJazdy;

import RozkladyJazdy.Model.Komunikat;
import RozkladyJazdy.Model.Przystanek;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author piotr
 */
@RunWith(JMockit.class)
public class AplikacjaMockDemo {
    @Mocked UslugaAutoryzacji mockUslugaAutoryzacji;
    @Mocked Komunikat mockKomunikat;
    
    @Test
    public void dodajPrzystanekNieDzialaJesliZnalezionoInnyOTejNazwie(){
        Aplikacja a = new Aplikacja(mockUslugaAutoryzacji);
        
        new Expectations(a){{
            mockUslugaAutoryzacji.sprawdzDostep(); result = true;
            a.szukajPrzystanek(anyString); result = new Przystanek();
        }};
        
        a.dodajLinie("1");
        a.dodajPrzystanek("test", "1");
        
        assertTrue(a.iloscPrzystankow() == 0);
    
        new Verifications() {{ 
            a.szukajPrzystanek(anyString);
        }};
    }
    
    @Test
    public void wyswietlKomunikatUzywaFunkcjiPobierzTresc(){
        Aplikacja a = new Aplikacja(mockUslugaAutoryzacji);
        
        new Expectations(a){{
            mockUslugaAutoryzacji.sprawdzDostep(); result = true;
        }};
        
        a.dodajKomunikat(0, "test");
        a.dodajKomunikat(1, "test2");
        a.dodajKomunikat(2, "test3");
        
        a.wyswietlWszystkieKomunikaty();
        
        new Verifications() {{ 
            mockKomunikat.pobierzTresc(); times = 3;
        }};
    }
    
    
}
