/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RozkladyJazdy;

import Suite.TestControl;
import Suite.TestEntity;
import RozkladyJazdy.Model.Przystanek;
import RozkladyJazdy.Model.Uzytkownik;
import Suite.TestDataEdit;
import mockit.Mocked;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;

@Category({TestControl.class, TestEntity.class})

public class AplikacjaTest {
    static Aplikacja instance;
        
    //@Mocked UslugaAutoryzacji mockUslugaAutoryzacji;
    
    @BeforeClass
    public static void setUpClass() {
        instance = new Aplikacja();
        Uzytkownik u = new Uzytkownik("pracownik1", "12345");
        instance.uslugaAutoryzacji.uzytkownicy.add(u);
        instance.uslugaAutoryzacji.weryfikujKlucz("pracownik1", "12345");
        
        instance.dodajLinie("1");
        instance.dodajPrzystanek("Przystanek1", "1");
        instance.dodajPrzystanek("Przystanek2", "1");
        instance.dodajPrzystanek("Przystanek3", "1");
        instance.uslugaAutoryzacji.zamknijDostep();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }


    /**
     * Test of szukajPrzystanek method, of class Aplikacja.
     */
    @Test
    public void testSzukajPrzystanek() {
        System.out.println("szukajPrzystanek");
        
        Przystanek result = instance.szukajPrzystanek("Przystanek1");
        assertEquals(result.pobierzNazwe(), "Przystanek1");
        
        Przystanek result2 = instance.szukajPrzystanek("Przystanek123456");
        assertEquals(result2, null);
    }



    /**
     * Test of dodajPrzystanek method, of class Aplikacja.
     */
    @Test
    @Category(TestDataEdit.class)
    public void testDodajPrzystanek() {
        System.out.println("dodajPrzystanek");
        
        instance.uslugaAutoryzacji.weryfikujKlucz("pracownik1", "12345");
        
        System.out.println("Il. przys. " + instance.iloscPrzystankow());
        assertTrue(instance.iloscPrzystankow() == 3);
        instance.dodajPrzystanek("Przystanek1", "1");
        assertTrue(instance.iloscPrzystankow() == 3);
        instance.dodajPrzystanek("Przystanek4", "1");
        assertTrue(instance.iloscPrzystankow() == 4);

        Przystanek przystanek = instance.szukajPrzystanek("Przystanek4");
        assertTrue(przystanek != null);
        
        instance.usunPrzystanek("Przystanek4", "1");
    }
 


    /**
     * Test of usunPrzystanek method, of class Aplikacja.
     */
    @Test
    @Category(TestDataEdit.class)
    public void testUsunPrzystanek() {
        System.out.println("usunPrzystanek");
        instance.uslugaAutoryzacji.weryfikujKlucz("pracownik1", "12345");

        instance.usunPrzystanek("Przystanek2", "1");
        
        assertEquals(null, instance.szukajPrzystanek("Przystanek2"));
        assertEquals(2, instance.iloscPrzystankow());
        assertEquals(true, instance.uslugaAutoryzacji.sprawdzDostep());
        
        instance.dodajPrzystanek("Przystanek2", "1");
        assertEquals(3, instance.iloscPrzystankow());
    }
    
}
