/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RozkladyJazdy;

import Suite.TestControl;
import RozkladyJazdy.Model.Uzytkownik;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;

@Category({TestControl.class})

public class UslugaAutoryzacjiTest {
    
    static UslugaAutoryzacji instance;
    
    @BeforeClass
    public static void setUpClass() {
        instance = new UslugaAutoryzacji();
        Uzytkownik u = new Uzytkownik("uzytkownik1", "haslo1");
        instance.uzytkownicy.add(u);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
        instance.zamknijDostep();
    }   

    /**
     * Test of weryfikujKlucz method, of class UslugaAutoryzacji.
     */
    @Test
    public void testWeryfikujKlucz() {
        System.out.println("weryfikujKlucz");
       
        instance.weryfikujKlucz("uzytkownik2", "haslo1");
        assertFalse(instance.sprawdzDostep());
        
        instance.weryfikujKlucz("uzytkownik1", "haslo2");
        assertFalse(instance.sprawdzDostep());
        
        instance.weryfikujKlucz("uzytkownik1", "haslo1");
        assertTrue(instance.sprawdzDostep());
    }

    /**
     * Test of sprawdzDostep method, of class UslugaAutoryzacji.
     */
    @Test
    public void testSprawdzDostep() {
        System.out.println("sprawdzDostep");
        boolean expResult = false;
        boolean result = instance.sprawdzDostep();
        assertEquals(expResult, result);
        
        instance.weryfikujKlucz("uzytkownik1", "haslo1");
        assertTrue(instance.sprawdzDostep());
    }
    
}
