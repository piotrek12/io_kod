/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RozkladyJazdy.Model;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 *
 * @author piotr
 */
public class LiniaTest_RuleDemo {
    static Linia instance;
    @Rule
    public ExpectedException exception = ExpectedException.none();
    
    public LiniaTest_RuleDemo() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = new Linia();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    
    @Test
    public void removingNullThrowsException() {
        exception.expect(NullPointerException.class);
        instance.usunPrzystanek(null);
    }
}
