/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RozkladyJazdy.Model;

import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class UzytkownikTest {
    
    @Parameterized.Parameter(0)
    public String login;

    @Parameterized.Parameter(1)
    public String realPassword;
    
    @Parameterized.Parameter(2)
    public String providedPassword;

    @Parameterized.Parameter(3)
    public boolean result;
    
    public UzytkownikTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }   
    
    @Parameterized.Parameters(
            name = "{index}: Test with login={0},"
                    + " realPassword ={1},"
                    + " providedPassword ={2},"
                    + " result={3}")
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{
            {"login1", "12345", "12345", true},
            {"login1", "12345", "123456", false},
            {"login1", "12345", "", false},
        };
        return Arrays.asList(data);
    }

    /**
     * Test of porownajHaslo method, of class Uzytkownik.
     */
    @Test
    public void testPorownajHaslo() {
        System.out.println("porownajHaslo");
        Uzytkownik instance = new Uzytkownik(login, realPassword);
        boolean expResult = result;
        boolean actResult = instance.porownajHaslo(providedPassword);
        assertEquals(expResult, actResult);
    }
    
}
