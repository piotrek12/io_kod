/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RozkladyJazdy;

import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;

/**
 *
 * @author piotr
 */
@RunWith(JMockit.class)
public class AplikacjaTest_UslugaAutoryzacjiMock {
    
    @Mocked UslugaAutoryzacji mockUslugaAutoryzacji;
    
    public AplikacjaTest_UslugaAutoryzacjiMock() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    @Test
    public void dodajPrzystanekNieDzialaBezLogowania(){
        new Expectations(){{
            mockUslugaAutoryzacji.sprawdzDostep(); result=false;
        }};
        
        Aplikacja p = new Aplikacja();
        p.uslugaAutoryzacji = mockUslugaAutoryzacji;
        p.dodajLinie("1");
        p.dodajPrzystanek("test", "1");
        
        assertTrue(p.iloscPrzystankow() == 0);
    }
    
    @Test
    public void dodajPrzystanekDzialaDlaUzytkownikowZalogowanych(){
        new Expectations(){{
            mockUslugaAutoryzacji.sprawdzDostep(); result=true;
        }};
        
        Aplikacja p = new Aplikacja();
        p.uslugaAutoryzacji = mockUslugaAutoryzacji;
        p.dodajLinie("1");
        p.dodajPrzystanek("test", "1");
        
        assertTrue(p.iloscPrzystankow() == 1);
    }
}
