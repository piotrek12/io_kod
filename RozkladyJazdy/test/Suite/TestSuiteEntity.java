/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Suite;

import RozkladyJazdy.AplikacjaTest;
import RozkladyJazdy.PrzystanekTest;
import RozkladyJazdy.UslugaAutoryzacjiTest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@Categories.SuiteClasses({AplikacjaTest.class, PrzystanekTest.class, UslugaAutoryzacjiTest.class})
@RunWith(Categories.class)
@Categories.IncludeCategory(TestEntity.class)

public class TestSuiteEntity {

    
}
