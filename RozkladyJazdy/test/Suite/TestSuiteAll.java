/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Suite;

import RozkladyJazdy.AplikacjaMockDemo;
import RozkladyJazdy.AplikacjaTest;
import RozkladyJazdy.AplikacjaTest_UslugaAutoryzacjiMock;
import RozkladyJazdy.Model.LiniaTest_RuleDemo;
import RozkladyJazdy.Model.UzytkownikTest;
import RozkladyJazdy.PrzystanekTest;
import RozkladyJazdy.UslugaAutoryzacjiTest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@SuiteClasses({
    AplikacjaMockDemo.class,
    AplikacjaTest.class,
    AplikacjaTest_UslugaAutoryzacjiMock.class,
    PrzystanekTest.class,
    UslugaAutoryzacjiTest.class,
    LiniaTest_RuleDemo.class,
    RozkladyJazdy.Model.PrzystanekTest.class,
    UzytkownikTest.class
})
@RunWith(Suite.class)

public class TestSuiteAll {


}
