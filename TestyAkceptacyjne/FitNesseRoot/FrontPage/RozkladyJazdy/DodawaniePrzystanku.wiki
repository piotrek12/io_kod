---
Test
---
!include DodawanieLinii
!| TestyFitnesseFixture.TestDodawaniePrzystankow |
| nazwaNowegoPrzystanku | liniaDoKtorejDodacPrzystanek | dodajPrzystanek? | iloscPrzystankow? |
| Przystanek1 | 1 | true | 1 |
| Przystanek2 | 1 | true | 2 |
| Przystanek3 | 1 | true | 3 |
| Przystanek1 | 1 |false | 3 |
| Przystanek1 | 2 | true | 3 |
